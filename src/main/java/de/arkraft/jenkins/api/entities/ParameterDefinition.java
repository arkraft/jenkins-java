package de.arkraft.jenkins.api.entities;

import de.arkraft.jenkins.api.JenkinsEntity;

/**
 * A ParameterDefinition is used by Jenkins to describe the "actions" (at least that's what i found out so far) and
 * "property" tag in a job response
 *
 * @author Artur Kraft
 */
public class ParameterDefinition implements JenkinsEntity {

    /**
     * Default values for this parameter
     */
    private ParameterDefinition defaultParameterValue;
    /**
     * Description of the parameter
     */
    private String description;
    /**
     * Name of the parameter
     */
    private String name;
    /**
     * Type of the parameter
     */
    private String type;

    /**
     * Returns the default ParameterDefinition
     *
     * @return defaultParameterValue
     */
    public ParameterDefinition getDefaultParameterValue() {
        return defaultParameterValue;
    }

    /**
     * Set the default parameter value
     *
     * @param defaultParameterValue new ParameterDefinition
     */
    public void setDefaultParameterValue(ParameterDefinition defaultParameterValue) {
        this.defaultParameterValue = defaultParameterValue;
    }

    /**
     * Get the description of this ParameterDefinition
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description of the ParameterDefinition
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the name of the ParameterDefinition
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the ParameterDefinition
     *
     * @param name new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the type
     *
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * Set the type
     *
     * @param type new type
     */
    public void setType(String type) {
        this.type = type;
    }
}
