package de.arkraft.jenkins.api.entities.helper;

import de.arkraft.jenkins.api.JenkinsEntity;

/**
 * Created by Artur on 30.05.2014.
 */
public class BuildRevision implements JenkinsEntity {

    private String SHA1;
    private Branch branch;

    public String getSHA1() {
        return SHA1;
    }

    public void setSHA1(String SHA1) {
        this.SHA1 = SHA1;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }
}
