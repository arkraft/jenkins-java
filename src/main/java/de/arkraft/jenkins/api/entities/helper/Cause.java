package de.arkraft.jenkins.api.entities.helper;

import de.arkraft.jenkins.api.JenkinsEntity;

import java.util.List;

/**
 * Created by Artur on 11.05.2014.
 */
public abstract class Cause implements JenkinsEntity {

    /**
     * Description
     */
    private String shortDescription;
    public abstract CauseType getType();

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public class UpstreamCause extends Cause {

        public final CauseType TYPE = CauseType.UPSTREAM;
        private String upstreamProject;
        private String upstreamUrl;
        private long upstreamBuild;

        public String getUpstreamProject() {
            return upstreamProject;
        }

        public void setUpstreamProject(String upstreamProject) {
            this.upstreamProject = upstreamProject;
        }

        public String getUpstreamUrl() {
            return upstreamUrl;
        }

        public void setUpstreamUrl(String upstreamUrl) {
            this.upstreamUrl = upstreamUrl;
        }

        public long getUpstreamBuild() {
            return upstreamBuild;
        }

        public void setUpstreamBuild(long upstreamBuild) {
            this.upstreamBuild = upstreamBuild;
        }

        @Override
        public CauseType getType() {
            return TYPE;
        }

    }

    public class RemoteCause extends Cause {

        public final CauseType TYPE = CauseType.REMOTE;
        private String addr;
        private String note;

        public String getAddr() {
            return addr;
        }

        public void setAddr(String addr) {
            this.addr = addr;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        @Override
        public CauseType getType() {
            return TYPE;
        }

    }

    public class UserIdCause extends Cause {

        public final CauseType TYPE = CauseType.USERID;
        private String userId;
        private String userName;

        @Override
        public CauseType getType() {
            return TYPE;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }
    }

    public class UserCause extends Cause {

        public final CauseType TYPE = CauseType.USER;
        private String userName;

        @Override
        public CauseType getType() {
            return TYPE;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }
    }

    public class LegacyCodeCause extends Cause {

        public final CauseType TYPE = CauseType.LEGACY_CODE;
        private List<String> stackTrace;

        @Override
        public CauseType getType() {
            return TYPE;
        }

        public List<String> getStackTrace() {
            return stackTrace;
        }

        public void setStackTrace(List<String> stackTrace) {
            this.stackTrace = stackTrace;
        }
    }

}
