package de.arkraft.jenkins.api;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import de.arkraft.jenkins.api.services.*;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.converter.GsonConverter;

/**
 * Main Class of a Jenkins Server
 *
 * @author Artur Kraft
 */
public class Jenkins {

    /**
     * Jenkins server URL
     */
    private String server;
    /**
     * Jenkins user for secured access
     */
    private String user;
    /**
     * Jenkins API Key for secured access (can be found in /users/me/configuration)
     */
    private String apiKey;
    /**
     * Retrofit RestAdapter
     */
    private RestAdapter restAdapter;
    /**
     * set debug flag
     */
    private boolean debug;

    /**
     * Create Manager instance
     *
     * @param server String server url
     */
    public Jenkins(String server) {
        this.server = server;
    }

    /**
     * Returns URL of the actual Jenkins server
     *
     * @return Url of the server
     */
    public String getServer() {
        return this.server;
    }

    /**
     * Set Jenkins Server
     *
     * @param server URL of the Jenkins Instance
     * @return Jenkins Server Object
     */
    public Jenkins setServer(String server) {
        this.server = server;
        this.restAdapter = null;
        return this;
    }

    /**
     * Turns on Authentication during connection to Jenkins Server
     *
     * @param user   username of the Jenkins user
     * @param apiKey apiKey for the Jenkins user (can be found in profile details)
     * @return Jenkins Server Object
     */
    public Jenkins withAuthentication(String user, String apiKey) {
        this.user = user;
        this.apiKey = apiKey;
        this.restAdapter = null;
        return this;
    }

    /**
     * If this is set to true, debug log messages will appear in the console
     *
     * @param debug true if you want debug messages
     * @return this Jenkins
     */
    public Jenkins withDebug(boolean debug) {
        this.debug = debug;
        this.restAdapter = null;
        return this;
    }

    /**
     * If no restAdapter has been set, create a new Restadapter with authentication and GsonBuilder
     *
     * @return RestAdapter
     */
    protected RestAdapter buildRestAdapter() {
        if (restAdapter == null) {
            RestAdapter.Builder builder = new RestAdapter.Builder().setEndpoint(server)
                    .setConverter(new GsonConverter(JenkinsHelper.getGsonBuilder().create()));

            // if available, send mUsername and password in header
            builder.setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade requestFacade) {
                    if (user != null && !user.equals("")) {
                        String encodedAuth = Base64.encode((user + ":" + apiKey).getBytes());
                        requestFacade.addHeader("Authorization", "Basic " + encodedAuth);
                    }
                }
            });

            builder.setErrorHandler(new ErrorHandler() {
                @Override
                public Throwable handleError(RetrofitError retrofitError) {
                    retrofit.client.Response response = retrofitError.getResponse();
                    if(response != null) {
                        switch (response.getStatus()) {
                            case 401:
                                return RetrofitException.UnauthorizedException(response.getReason(), retrofitError);
                        }
                    }
                    return retrofitError;
                }
            });

            if (debug) {
                builder.setLogLevel(RestAdapter.LogLevel.FULL);
            }

            restAdapter = builder.build();
        }

        return restAdapter;
    }

    public JobService getJobService() {
        return buildRestAdapter().create(JobService.class);
    }

    public ServerService getServerService() {
        return buildRestAdapter().create(ServerService.class);
    }

    public NodeService getNodeService() {
        return buildRestAdapter().create(NodeService.class);
    }

    public UserService getUserService() { return buildRestAdapter().create(UserService.class); }

    public ViewService getViewService() { return buildRestAdapter().create(ViewService.class); }

}
