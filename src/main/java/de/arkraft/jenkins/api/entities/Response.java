package de.arkraft.jenkins.api.entities;

import de.arkraft.jenkins.api.JenkinsEntity;

/**
 * Empty Response for BuildService because Retrofit needs a return type or a callback
 *
 * @author Artur Kraft
 */
public class Response implements JenkinsEntity {
}
