package de.arkraft.jenkins.api.entities;

import com.google.gson.annotations.SerializedName;
import de.arkraft.jenkins.api.JenkinsEntity;

/**
 * Monitoring data for a Computer.
 * <p/>
 * TODO: Check for monitoring data not in this list
 *
 * @author Artur Kraft
 */
public class MonitorData implements JenkinsEntity {

    /**
     * Swap space monitor
     */
    @SerializedName("hudson.node_monitors.SwapSpaceMonitor")
    private SwapSpaceMonitor swapSpaceMonitor;
    /**
     * Architecture monitor
     */
    @SerializedName("hudson.node_monitors.ArchitectureMonitor")
    private String architectureMonitor;
    /**
     * Temporary space monitor
     */
    @SerializedName("hudson.node_monitors.TemporarySpaceMonitor")
    private SpaceMonitor temporarySpaceMonitor;
    /**
     * Disk space monitor
     */
    @SerializedName("hudson.node_monitors.DiskSpaceMonitor")
    private SpaceMonitor diskSpaceMonitor;
    /**
     * Clock monitor
     */
    @SerializedName("hudson.node_monitors.ClockMonitor")
    private ClockMonitor clockMonitor;
    /**
     * Response time monitor
     */
    @SerializedName("hudson.node_monitors.ResponseTimeMonitor")
    private ResponseTimeMonitor responseTimeMonitor;

    /**
     * Returns the ResponseTimeMonitor. It keeps the average response time
     *
     * @return responseTimeMonitor
     */
    public ResponseTimeMonitor getResponseTimeMonitor() {
        return responseTimeMonitor;
    }

    /**
     * Set the ResponseTimeMonitor
     *
     * @param responseTimeMonitor the new ResponseTimeMonitor
     */
    public void setResponseTimeMonitor(ResponseTimeMonitor responseTimeMonitor) {
        this.responseTimeMonitor = responseTimeMonitor;
    }

    /**
     * Get the SwapSpaceMonitor. It keeps the swap space availability
     *
     * @return swapSpaceMonitor
     */
    public SwapSpaceMonitor getSwapSpaceMonitor() {
        return swapSpaceMonitor;
    }

    /**
     * Set the SwapSpaceMonitor
     *
     * @param swapSpaceMonitor new SwapSpaceMonitor
     */
    public void setSwapSpaceMonitor(SwapSpaceMonitor swapSpaceMonitor) {
        this.swapSpaceMonitor = swapSpaceMonitor;
    }

    /**
     * Get the ArchitectureMonitor. It keeps the architecture of the system to display.
     *
     * @return architectureMonitor
     */
    public String getArchitectureMonitor() {
        return architectureMonitor;
    }

    /**
     * Set the ArchitectureMonitor
     *
     * @param architectureMonitor new ArchitectureMonitor
     */
    public void setArchitectureMonitor(String architectureMonitor) {
        this.architectureMonitor = architectureMonitor;
    }

    /**
     * Get the TemporarySpaceMonitor. It keeps the available space in the <i>"/tmp"</i> directory
     *
     * @return temporarySpaceMonitor
     */
    public SpaceMonitor getTemporarySpaceMonitor() {
        return temporarySpaceMonitor;
    }

    /**
     * Set the TemporarySpaceMonitor
     *
     * @param temporarySpaceMonitor the new temporarySpaceMonitor
     */
    public void setTemporarySpaceMonitor(SpaceMonitor temporarySpaceMonitor) {
        this.temporarySpaceMonitor = temporarySpaceMonitor;
    }

    /**
     * Get the DiskSpaceMonitor. It keeps the available disk space in the computers root
     *
     * @return diskSpaceMonitor
     */
    public SpaceMonitor getDiskSpaceMonitor() {
        return diskSpaceMonitor;
    }

    /**
     * Set the diskSpaceMonitor
     *
     * @param diskSpaceMonitor the new DiskSpaceMonitor
     */
    public void setDiskSpaceMonitor(SpaceMonitor diskSpaceMonitor) {
        this.diskSpaceMonitor = diskSpaceMonitor;
    }

    /**
     * Get the ClockMonitor. It keeps the difference in clock time.
     *
     * @return clockMonitor
     */
    public ClockMonitor getClockMonitor() {
        return clockMonitor;
    }

    /**
     * Set the ClockMonitor
     *
     * @param clockMonitor the new ClockMonitor
     */
    public void setClockMonitor(ClockMonitor clockMonitor) {
        this.clockMonitor = clockMonitor;
    }

    /**
     * Object that represents a SwapSpaceMonitor Object in Jenkins
     */
    private class SwapSpaceMonitor implements JenkinsEntity {

        /**
         * Available physical space
         */
        private long availablePhysicalMemory;
        /**
         * Available swap space
         */
        private long availableSwapSpace;
        /**
         * Total physical space
         */
        private long totalPhysicalMemory;
        /**
         * Total swap space
         */
        private long totalSwapSpace;

        /**
         * Get the available (left) physical space
         *
         * @return {@link #availablePhysicalMemory}
         */
        public long getAvailablePhysicalMemory() {
            return availablePhysicalMemory;
        }

        /**
         * Set the available Physical Memory
         *
         * @param availablePhysicalMemory the value to set
         */
        public void setAvailablePhysicalMemory(long availablePhysicalMemory) {
            this.availablePhysicalMemory = availablePhysicalMemory;
        }

        /**
         * Get the available (left) swap space
         *
         * @return {@link #availableSwapSpace}
         */
        public long getAvailableSwapSpace() {
            return availableSwapSpace;
        }

        /**
         * Set the available swap space
         *
         * @param availableSwapSpace value to set
         */
        public void setAvailableSwapSpace(long availableSwapSpace) {
            this.availableSwapSpace = availableSwapSpace;
        }

        /**
         * Get the total physical space
         *
         * @return {@link #totalPhysicalMemory}
         */
        public long getTotalPhysicalMemory() {
            return totalPhysicalMemory;
        }

        /**
         * Set the total physical space
         *
         * @param totalPhysicalMemory calue to set
         */
        public void setTotalPhysicalMemory(long totalPhysicalMemory) {
            this.totalPhysicalMemory = totalPhysicalMemory;
        }

        /**
         * Get the total swap space
         *
         * @return {@link #totalSwapSpace}
         */
        public long getTotalSwapSpace() {
            return totalSwapSpace;
        }

        /**
         * Set the total swap space
         *
         * @param totalSwapSpace value to set
         */
        public void setTotalSwapSpace(long totalSwapSpace) {
            this.totalSwapSpace = totalSwapSpace;
        }
    }

    /**
     * Object that represents a SpaceMonitor Node in Jenkins
     */
    private class SpaceMonitor implements JenkinsEntity {

        /**
         * Path to the root directory
         */
        private String path;
        /**
         * Available space in the root directory
         */
        private long size;

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public long getSize() {
            return size;
        }

        public void setSize(long size) {
            this.size = size;
        }
    }

    /**
     * Object that represents a ClockMonitor Node in Jenkins
     */
    private class ClockMonitor implements JenkinsEntity {

        /**
         * Time difference
         */
        private int diff;

        /**
         * Get the time difference
         *
         * @return diff
         */
        public int getDiff() {
            return diff;
        }

        /**
         * Set the time difference
         *
         * @param diff time difference to set
         */
        public void setDiff(int diff) {
            this.diff = diff;
        }
    }

    /**
     * Object that represents a ResponseTimeMonitor Node in Jenkins
     */
    private class ResponseTimeMonitor implements JenkinsEntity {

        /**
         * Average response time
         */
        private int average;

        /**
         * Get the average response time
         *
         * @return average response time
         */
        public int getAverage() {
            return average;
        }

        /**
         * set the average response time
         *
         * @param average the response time to set
         */
        public void setAverage(int average) {
            this.average = average;
        }
    }

}

