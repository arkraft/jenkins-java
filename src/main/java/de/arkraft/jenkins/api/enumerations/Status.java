package de.arkraft.jenkins.api.enumerations;

import de.arkraft.jenkins.api.JenkinsEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * Build result
 *
 * @author Artur Kraft
 */
public enum Status implements JenkinsEntity {

    FAILED("red", "Failed"),
    FAILED_ANIM("red_anime", "Failed building"),
    UNSTABLE("yellow", "Unstable"),
    UNSTABLE_ANIM("yellow_anime", "Unstable building"),
    SUCCESS("blue", "Success"),
    SUCCESS_ANIM("blue_anime", "Success building"),
    PENDING("grey","Pending"),
    PENDING_ANIM("grey_anime", "Pending building"),
    DISABLED("disabled","Disabled"),
    DISABLED_ANIM("disabled_anime", "Disabled building"),
    ABORTED("aborted","Aborted"),
    ABORTED_ANIM("aborted_anime", "Aborted building"),
    NOTBUILT("notbuilt","Notbuilt"),
    NOTBUILT_ANIM("notbuilt_anime", "Notbuilt building");

    private String color;
    private String name;
    private boolean building;
    private static final Map<String, Status> colorValuePairs = new HashMap<String, Status>();

    static {
        for (Status status : Status.values()) {
            colorValuePairs.put(status.getColor(), status);
        }
    }

    private Status(String color, String name) {
        this.color = color;
        this.name = name;
        this.building = color.endsWith("_anim");
    }

    public String getName() {
        return this.name;
    }

    public String getColor() {
        return this.color;
    }

    public static Status getByColor(String color) {
        return colorValuePairs.get(color);
    }

    public boolean isBuilding() {
        return this.building;
    }

}