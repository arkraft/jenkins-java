package de.arkraft.jenkins.api.entities;

import de.arkraft.jenkins.api.JenkinsEntity;

/**
 * Report about status of tests and build
 *
 * @author Artur Kraft
 */
public class HealthReport implements JenkinsEntity {

    private String description;
    private String iconUrl;
    private int score;

    /**
     * Returns full text version of health report
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description
     *
     * @param description new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get url to the icon representing the health status
     *
     * @return urlIcon
     */
    public String getIconUrl() {
        return iconUrl;
    }

    /**
     * Set url of the icon
     *
     * @param iconUrl new url
     */
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    /**
     * Get the reached score (1-100)
     *
     * @return score
     */
    public int getScore() {
        return score;
    }

    /**
     * Set the score
     *
     * @param score value between 1 and 100
     */
    public void setScore(int score) {
        this.score = score;
    }
}
