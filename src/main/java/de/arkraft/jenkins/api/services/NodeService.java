package de.arkraft.jenkins.api.services;

import de.arkraft.jenkins.api.entities.Node;
import de.arkraft.jenkins.api.entities.Nodes;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Artur on 14.05.2014.
 */
public interface NodeService {

    @GET("/computer/api/json")
    public Nodes getNodes();

    @GET("/computer/{name}/api/json?depth=2")
    public Node getNode(@Path("name") String name);

}
