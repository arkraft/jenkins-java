package de.arkraft.jenkins.api.entities;

import de.arkraft.jenkins.api.JenkinsEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * A view is a collection of jobs can be represented as a directory. It can contain jobs but also other views.
 *
 * @author Artur Kraft
 */
public class View implements JenkinsEntity {

    /**
     * Name of the view
     */
    private String name;
    /**
     * Url which can be called in a browser
     */
    private String url;
    /**
     * List of views
     */
    private List<View> views = new ArrayList<View>();
    /**
     * List of jobs
     */
    private List<Job> jobs = new ArrayList<Job>();

    /**
     * Get the name of the view
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the view
     *
     * @param name new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the Url to open the view in the browser
     *
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Set the url of the view
     *
     * @param url new url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Get the list of views inside this view
     *
     * @return views
     */
    public List<View> getViews() {
        return views;
    }

    /**
     * Set the list of views
     *
     * @param views new list
     */
    public void setViews(List<View> views) {
        this.views = views;
    }

    /**
     * Get the list of jobs inside this view
     *
     * @return jobs
     */
    public List<Job> getJobs() {
        return jobs;
    }

    /**
     * Set the list of jobs
     *
     * @param jobs new list
     */
    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }
}
