package de.arkraft.jenkins.api.entities;

import com.google.gson.annotations.SerializedName;
import de.arkraft.jenkins.api.JenkinsEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that represents a Executor entity. A Jenkins server can have multiple Executors which execute the builds
 *
 * @author Artur Kraft
 */
public class Nodes implements JenkinsEntity {

    @SerializedName("totalExecutors")
    private int executorsSize;
    private int busyExecutors;
    private String displayName;
    private List<Node> computer = new ArrayList<Node>();

    public int getExecutorsSize() {
        return executorsSize;
    }

    public void setExecutorsSize(int executorsSize) {
        this.executorsSize = executorsSize;
    }

    public int getBusyExecutors() {
        return busyExecutors;
    }

    public void setBusyExecutors(int busyExecutors) {
        this.busyExecutors = busyExecutors;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<Node> getComputer() {
        return computer;
    }

    public void setComputer(List<Node> computer) {
        this.computer = computer;
    }
}
