package de.arkraft.jenkins.api.entities;

import de.arkraft.jenkins.api.JenkinsEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * A set of changes
 */
public class ChangeSet implements JenkinsEntity {

    /**
     * List of changes
     */
    private List<ChangeItem> items = new ArrayList<ChangeItem>();
    /**
     * Kind of change
     */
    private String kind;
    /**
     * List of revisions
     */
    private List<Revision> revisions = new ArrayList<Revision>();

    /**
     * Return the list of changes
     *
     * @return changes
     */
    public List<ChangeItem> getItems() {
        return items;
    }

    /**
     * Set the list of changes
     *
     * @param items changes
     */
    public void setItems(List<ChangeItem> items) {
        this.items = items;
    }

    /**
     * Get the kind of change
     *
     * @return kind
     */
    public String getKind() {
        return kind;
    }

    /**
     * Set the kind of change
     *
     * @param kind chnage
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    /**
     * List of affected revisions
     *
     * @return revisions
     */
    public List<Revision> getRevisions() {
        return revisions;
    }

    /**
     * Set list of revisions
     *
     * @param revisions new revisions
     */
    public void setRevisions(List<Revision> revisions) {
        this.revisions = revisions;
    }

    /**
     * Represents a single revision
     */
    private class Revision implements JenkinsEntity {

        /**
         * Name of the module that was changed
         */
        private String module;
        /**
         * Revision of this module
         */
        private long revision;

        /**
         * Returns the url of the changed module
         *
         * @return url
         */
        public String getModule() {
            return module;
        }

        /**
         * Sets the url of the module
         *
         * @param module new value
         */
        public void setModule(String module) {
            this.module = module;
        }

        /**
         * Returns the new revision of the module
         *
         * @return revision
         */
        public long getRevision() {
            return revision;
        }

        /**
         * Sets the revision of the module
         *
         * @param revision revision
         */
        public void setRevision(long revision) {
            this.revision = revision;
        }
    }
}
