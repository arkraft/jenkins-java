package de.arkraft.jenkins.api.enumerations;

import de.arkraft.jenkins.api.JenkinsEntity;

/**
 * Represents the type of a change. A file can be added, deleted or edited.
 */
public enum ChangeType implements JenkinsEntity {

    ADD,
    EDIT,
    DELETE

}
