package de.arkraft.jenkins.api.entities;

import com.google.gson.annotations.SerializedName;
import de.arkraft.jenkins.api.JenkinsEntity;
import de.arkraft.jenkins.api.enumerations.Mode;

import java.util.ArrayList;
import java.util.List;

/**
 * Root node of jenkins server api
 *
 * @author Artur Kraft
 */
public class JenkinsServer implements JenkinsEntity {

    /**
     * Mode: NORMAL or EXPLICIT
     */
    private Mode mode;
    /**
     * Description for the node
     */
    private String nodeDescription;
    /**
     * Name of the Node
     */
    private String nodeName;
    /**
     * Number of Executioners for this server
     */
    @SerializedName("numExecutors") private int numberOfExecutors;
    /**
     * Description of the Server
     */
    private String description;
    /**
     * List of all views (without nested views)
     */
    private List<View> views = new ArrayList<View>();
    /**
     * Primary view, the one tab that is selected by default
     */
    private View primaryView;
    /**
     * List of all jobs
     */
    private List<Job> jobs = new ArrayList<Job>();
    /**
     * is server quieting down
     */
    private boolean quietingDown;
    /**
     * Slave agent port
     */
    private int slaveAgentPort;
    /**
     * Are crumbs used to secure the server from CSRF attacks
     */
    private boolean useCrumbs;
    /**
     * Is authentication enabled
     */
    private boolean useSecurity;


    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public String getNodeDescription() {
        return nodeDescription;
    }

    public void setNodeDescription(String nodeDescription) {
        this.nodeDescription = nodeDescription;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public int getNumberOfExecutors() {
        return numberOfExecutors;
    }

    public void setNumberOfExecutors(int numberOfExecutors) {
        this.numberOfExecutors = numberOfExecutors;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<View> getViews() {
        return views;
    }

    public void setViews(List<View> views) {
        this.views = views;
    }

    public View getPrimaryView() {
        return primaryView;
    }

    public void setPrimaryView(View primaryView) {
        this.primaryView = primaryView;
    }

    public List<Job> getJobs() {
        return jobs;
    }

    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }

    public boolean isQuietingDown() {
        return quietingDown;
    }

    public void setQuietingDown(boolean quietingDown) {
        this.quietingDown = quietingDown;
    }

    public int getSlaveAgentPort() {
        return slaveAgentPort;
    }

    public void setSlaveAgentPort(int slaveAgentPort) {
        this.slaveAgentPort = slaveAgentPort;
    }

    public boolean isUseCrumbs() {
        return useCrumbs;
    }

    public void setUseCrumbs(boolean useCrumbs) {
        this.useCrumbs = useCrumbs;
    }

    public boolean isUseSecurity() {
        return useSecurity;
    }

    public void setUseSecurity(boolean useSecurity) {
        this.useSecurity = useSecurity;
    }
}
