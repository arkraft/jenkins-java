package de.arkraft.jenkins.api;

import java.io.Serializable;

/**
 * <pre>
 * $Id: $
 * $LastChangedBy: $
 * $LastChangedRevision:  $
 * $LastChangedDate: $
 * </pre>
 *
 * @author Artur Kraft
 */
public interface JenkinsEntity extends Serializable {
}
