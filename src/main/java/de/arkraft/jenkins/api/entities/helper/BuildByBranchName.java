package de.arkraft.jenkins.api.entities.helper;

import com.google.gson.annotations.SerializedName;
import de.arkraft.jenkins.api.JenkinsEntity;

import java.util.List;

/**
 * Created by Artur on 30.05.2014.
 */
public class BuildByBranchName implements JenkinsEntity {

    @SerializedName(value = "origin/master")
    private Origin origin;
    private BuildRevision lastBuldRevision;
    private List<String> remoteUrls;
    private String scmName;

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public BuildRevision getLastBuldRevision() {
        return lastBuldRevision;
    }

    public void setLastBuldRevision(BuildRevision lastBuldRevision) {
        this.lastBuldRevision = lastBuldRevision;
    }

    public List<String> getRemoteUrls() {
        return remoteUrls;
    }

    public void setRemoteUrls(List<String> remoteUrls) {
        this.remoteUrls = remoteUrls;
    }

    public String getScmName() {
        return scmName;
    }

    public void setScmName(String scmName) {
        this.scmName = scmName;
    }
}
