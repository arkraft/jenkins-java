package de.arkraft.jenkins.api.enumerations;

import de.arkraft.jenkins.api.JenkinsEntity;

/**
 * Heath Status of a build
 *
 * @author Artur Kraft
 */
public enum Health implements JenkinsEntity {

    HEALTH_0_20(0, 20),
    HEALTH_21_40(21, 40),
    HEALTH_41_60(41, 60),
    HEALTH_61_80(61, 80),
    HEALTH_81_100(81, 100);

    private int min;
    private int max;

    private Health(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
}
