package de.arkraft.jenkins.api.entities;

import de.arkraft.jenkins.api.JenkinsEntity;

/**
 * Artifact Node in Jenkins json Response
 *
 * @author Artur Kraft
 */
public class Artifact implements JenkinsEntity {

    /**
     * Display path
     */
    private String displayPath;
    /**
     * Name of the file
     */
    private String fileName;
    /**
     * Relative path
     */
    private String relativePath;

    /**
     * Get the display path of the artifact
     *
     * @return displayPath
     */
    public String getDisplayPath() {
        return displayPath;
    }

    /**
     * Sets the display path
     *
     * @param displayPath new display path
     */
    public void setDisplayPath(String displayPath) {
        this.displayPath = displayPath;
    }

    /**
     * Get the name of the file
     *
     * @return fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the name of the file
     *
     * @param fileName new file name
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Get the relative path
     *
     * @return relativePath
     */
    public String getRelativePath() {
        return relativePath;
    }

    /**
     * Sets the relative Path
     *
     * @param relativePath new relative path
     */
    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }
}
