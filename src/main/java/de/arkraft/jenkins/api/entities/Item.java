package de.arkraft.jenkins.api.entities;

import com.google.gson.annotations.SerializedName;
import de.arkraft.jenkins.api.JenkinsEntity;

import java.util.Date;

/**
 * An Item in the JobQueue
 *
 * @author Artur Kraft
 */
public class Item implements JenkinsEntity {

    private boolean blocked;
    private boolean buildable;
    private int id;
    private Date inQueueSince;
    private String params;
    private boolean stuck;
    private String url;
    private String why;
    @SerializedName("buildableStartMilliseconds")
    private Date buildableStart;
    private boolean pending;
    private Job task;

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean isBuildable() {
        return buildable;
    }

    public void setBuildable(boolean buildable) {
        this.buildable = buildable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getInQueueSince() {
        return inQueueSince;
    }

    public void setInQueueSince(Date inQueueSince) {
        this.inQueueSince = inQueueSince;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public boolean isStuck() {
        return stuck;
    }

    public void setStuck(boolean stuck) {
        this.stuck = stuck;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWhy() {
        return why;
    }

    public void setWhy(String why) {
        this.why = why;
    }

    public Date getBuildableStart() {
        return buildableStart;
    }

    public void setBuildableStart(Date buildableStart) {
        this.buildableStart = buildableStart;
    }

    public boolean isPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }

    public Job getTask() {
        return task;
    }

    public void setTask(Job task) {
        this.task = task;
    }
}
