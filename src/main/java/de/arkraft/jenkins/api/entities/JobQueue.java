package de.arkraft.jenkins.api.entities;

import de.arkraft.jenkins.api.JenkinsEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * The JobQueue holds the list of queuesd items
 *
 * @author Artur Kraft
 */
public class JobQueue implements JenkinsEntity {

    /**
     * List of Items in the JobQueue
     */
    private List<Item> items = new ArrayList<Item>();

    /**
     * Returns List of Queue Items
     * @return List of Queue Items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * Set Queue Items
     * @param items List of Items for the Queue
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

}
