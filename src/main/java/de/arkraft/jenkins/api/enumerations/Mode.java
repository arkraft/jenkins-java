package de.arkraft.jenkins.api.enumerations;

import de.arkraft.jenkins.api.JenkinsEntity;

/**
 * Execution Mode. Really don't know what this is for
 *
 * @author Artur Kraft
 */
public enum Mode implements JenkinsEntity {

    NORMAL,
    EXCLUSIVE

}
