package de.arkraft.jenkins.api;

/**
 * Created by Artur on 07.05.2014.
 */
public class RetrofitException extends RuntimeException {

    public RetrofitException(Throwable cause) {
        super(cause);
    }

    public RetrofitException(String message, Throwable cause) {
        super(message, cause);
    }

    public static RetrofitException UnauthorizedException(String reason, Throwable cause) {
        return new RetrofitException(reason, cause);
    }

}
