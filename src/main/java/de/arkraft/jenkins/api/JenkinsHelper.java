package de.arkraft.jenkins.api;

import com.google.gson.*;
import de.arkraft.jenkins.api.entities.helper.CauseAction;
import de.arkraft.jenkins.api.entities.helper.Action;
import de.arkraft.jenkins.api.entities.helper.BuildAction;
import de.arkraft.jenkins.api.entities.helper.Cause;
import de.arkraft.jenkins.api.entities.helper.TestReportAction;
import de.arkraft.jenkins.api.enumerations.ChangeType;
import de.arkraft.jenkins.api.enumerations.Mode;
import de.arkraft.jenkins.api.enumerations.Status;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Jenkins Helper to register custom adapters for GsonBuilder
 *
 * @author Artur Kraft
 */
public class JenkinsHelper {

    private static final SimpleDateFormat[] DATEFORMATS = {
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSS'Z'"),
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ"),
            new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss")
    };


    public static GsonBuilder getGsonBuilder() {
        GsonBuilder builder = new GsonBuilder();

        //region Mode Adapter
        builder.registerTypeAdapter(Mode.class, new JsonDeserializer<Mode>() {
            @Override
            public Mode deserialize(
                    JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return Mode.valueOf(json.getAsString());
            }
        });
        builder.registerTypeAdapter(Mode.class, new JsonSerializer<Mode>() {
            @Override
            public JsonElement serialize(
                    Mode src, Type typeOfSrc, JsonSerializationContext context) {
                return new JsonPrimitive(src.toString());
            }
        });
        //endregion

        //region Status Adapter
        builder.registerTypeAdapter(Status.class, new JsonDeserializer<Status>() {
            @Override
            public Status deserialize(
                    JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return Status.getByColor(json.getAsString());
            }
        });

        builder.registerTypeAdapter(Status.class, new JsonSerializer<Status>() {
            @Override
            public JsonElement serialize(
                    Status src, Type typeOfSrc, JsonSerializationContext context) {
                return new JsonPrimitive(src.getColor());
            }
        });
        //endregion

        //region BuildDate Adapter
        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

            @Override
            public Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
                try {
                    return new Date(jsonElement.getAsLong());
                } catch (NumberFormatException outer) {
                    JsonParseException jsonParseException = null;
                    for (SimpleDateFormat jenkinsBuildDateformat : DATEFORMATS) {
                        try {
                            return jenkinsBuildDateformat.parse(jsonElement.getAsString());
                        } catch (ParseException inner) {
                            jsonParseException = new JsonParseException(outer);
                        }
                    }
                    if(jsonParseException != null) {
                        throw jsonParseException;
                    }
                    return null;
                }
            }
        });
        //endregion

        //region ChangeType Adapter
        builder.registerTypeAdapter(ChangeType.class, new JsonDeserializer<ChangeType>() {
            @Override
            public ChangeType deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
                for (ChangeType changeType : ChangeType.values()) {
                    if (changeType.name().toLowerCase().equals(jsonElement.getAsString())) {
                        return changeType;
                    }
                }
                return null;
            }
        });

        builder.registerTypeAdapter(ChangeType.class, new JsonSerializer<ChangeType>() {
            @Override
            public JsonElement serialize(ChangeType changeType, Type type, JsonSerializationContext jsonSerializationContext) {
                return new JsonPrimitive(changeType.name());
            }
        });
        //endregion

        //region Cause
        builder.registerTypeAdapter(Cause.class, new JsonDeserializer<Cause>() {
            @Override
            public Cause deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
                if (jsonElement.getAsJsonObject().has("userId")) {
                    return jsonDeserializationContext.deserialize(jsonElement, Cause.UserIdCause.class);
                } else if (jsonElement.getAsJsonObject().has("stackTrace")) {
                    return jsonDeserializationContext.deserialize(jsonElement, Cause.LegacyCodeCause.class);
                } else if (jsonElement.getAsJsonObject().has("upstreamUrl")) {
                    return jsonDeserializationContext.deserialize(jsonElement, Cause.UpstreamCause.class);
                } else if (jsonElement.getAsJsonObject().has("addr")) {
                    return jsonDeserializationContext.deserialize(jsonElement, Cause.RemoteCause.class);
                } else {
                    return jsonDeserializationContext.deserialize(jsonElement, Cause.UserCause.class);
                }
            }
        });
        //endregion

        builder.registerTypeAdapter(Action.class, new JsonDeserializer<Action>() {
            @Override
            public Action deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
                if (((JsonObject) jsonElement).get("causes") != null) {
                    return jsonDeserializationContext.deserialize(jsonElement, CauseAction.class);
                } else if (((JsonObject) jsonElement).get("totalCount") != null) {
                    return jsonDeserializationContext.deserialize(jsonElement, TestReportAction.class);
                } else if (((JsonObject) jsonElement).get("buildsByBranchName") != null) {
                    return jsonDeserializationContext.deserialize(jsonElement, BuildAction.class);
                }
                return null;
            }
        });

        return builder;
    }

}
