package de.arkraft.jenkins.api.services;

import de.arkraft.jenkins.api.entities.User;
import de.arkraft.jenkins.api.entities.Users;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Artur on 30.05.2014.
 */
public interface UserService {

    @GET("/asynchPeople/api/json")
    public Users listUsers();

    @GET("/user/{id}/api/json")
    public User get(@Path("id") String userName);

}
