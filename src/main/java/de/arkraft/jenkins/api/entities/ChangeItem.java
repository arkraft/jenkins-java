package de.arkraft.jenkins.api.entities;

import com.google.gson.annotations.SerializedName;
import de.arkraft.jenkins.api.JenkinsEntity;
import de.arkraft.jenkins.api.enumerations.ChangeType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represents a single item in a ChangeSet
 */
public class ChangeItem implements JenkinsEntity {

    /**
     * Relative paths to the affected files
     */
    private List<String> affectedPaths = new ArrayList<String>();
    /**
     * Author
     */
    private User author;
    /**
     * Id of the commit
     */
    private String commitId;
    /**
     * Date when this build was triggered
     */
    private Date date;
    /**
     * Last commit message
     */
    @SerializedName("msg")
    private String commitMessage;
    /**
     * Paths to files that were changed
     */
    private List<Path> paths = new ArrayList<Path>();
    /**
     * Revision number
     */
    private String revision;
    /**
     * User who did the changes/triggered the build
     */
    private User user;
    /**
     * Timestamp of when the build was triggered, i think
     */
    private long timestamp;

    /**
     * Returns a list with all affected files
     *
     * @return affectedPaths
     */
    public List<String> getAffectedPaths() {
        return affectedPaths;
    }

    /**
     * Sets the affected files
     *
     * @param affectedPaths new list
     */
    public void setAffectedPaths(List<String> affectedPaths) {
        this.affectedPaths = affectedPaths;
    }

    /**
     * Get the author of the change
     *
     * @return author
     */
    public User getAuthor() {
        return author;
    }

    /**
     * Set the author of the change
     *
     * @param author author
     */
    public void setAuthor(User author) {
        this.author = author;
    }

    /**
     * Get the commit id
     *
     * @return commitId
     */
    public String getCommitId() {
        return commitId;
    }

    /**
     * Set the commitId
     *
     * @param commitId new id
     */
    public void setCommitId(String commitId) {
        this.commitId = commitId;
    }

    /**
     * Return the date the build was triggered
     *
     * @return date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets the date the build was triggered
     *
     * @param date new date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Get the commit message
     *
     * @return commitMessage
     */
    public String getCommitMessage() {
        return commitMessage;
    }

    /**
     * Sets the commit message
     *
     * @param commitMessage new message
     */
    public void setCommitMessage(String commitMessage) {
        this.commitMessage = commitMessage;
    }

    /**
     * List of files that were changed and the type of change
     *
     * @return paths
     */
    public List<Path> getPaths() {
        return paths;
    }

    /**
     * Set paths to changes
     *
     * @param paths paths
     */
    public void setPaths(List<Path> paths) {
        this.paths = paths;
    }

    /**
     * Return the revision
     *
     * @return revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Set teh revision
     *
     * @param revision new revision
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * Get the user
     *
     * @return user
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the user
     *
     * @param user user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Get the timestamp
     *
     * @return timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Set the timestamp
     *
     * @param timestamp new timestamp
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * A path is the path to the file and the type of change occurred with this file
     */
    private class Path implements JenkinsEntity {

        /**
         * EDIT, ADD or DELETE
         */
        @SerializedName("editType")
        private ChangeType changeType;
        /**
         * Path of the file that was changed
         */
        private String file;

        /**
         * Return type of change
         *
         * @return editType
         */
        public ChangeType getChangeType() {
            return changeType;
        }

        /**
         * Set the type of change
         *
         * @param changeType new type
         */
        public void setChangeType(ChangeType changeType) {
            this.changeType = changeType;
        }

        /**
         * Get the relative path to the file
         *
         * @return filepath
         */
        public String getFile() {
            return file;
        }

        /**
         * Set the path to the file
         *
         * @param file path
         */
        public void setFile(String file) {
            this.file = file;
        }
    }

}
