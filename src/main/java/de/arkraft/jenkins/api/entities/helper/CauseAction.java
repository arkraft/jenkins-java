package de.arkraft.jenkins.api.entities.helper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artur on 27.05.2014.
 */
public class CauseAction implements Action {

    private List<Cause> causes = new ArrayList<Cause>();

    public List<Cause> getCauses() {
        return causes;
    }

    public void setCauses(List<Cause> causes) {
        this.causes = causes;
    }
}
