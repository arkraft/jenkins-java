package de.arkraft.jenkins.api.entities;

import com.google.gson.annotations.SerializedName;
import de.arkraft.jenkins.api.JenkinsEntity;
import de.arkraft.jenkins.api.enumerations.Status;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a Jenkins Job. It contains all important data like the builds and the health report.
 *
 * @author Artur Kraft
 */
public class Job implements JenkinsEntity {

    /**
     * Name of the Job
     */
    private String name;
    /**
     * Url to open the Job in a browser
     */
    private String url;
    /**
     * Status of the Job (represented by color)
     */
    @SerializedName("color")
    private Status status;
    /**
     * Job description
     */
    private String description;
    /**
     * Display name
     */
    private String displayName;
    /**
     * No Idea what this means
     */
    private String displayNameOrNull;
    /**
     * can this job be build
     */
    private boolean buildable;
    /**
     * List of actions
     */
    private List<Action> actions = new ArrayList<Action>();
    /**
     * List of properties
     */
    private List<Action> property = new ArrayList<Action>();
    /**
     * List of saved and current builds
     */
    private List<Build> builds = new ArrayList<Build>();
    /**
     * First build of this Job
     */
    private Build firstBuild;
    /**
     * List of health reports (actually, i never saw more than two)
     */
    private List<HealthReport> healthReport = new ArrayList<HealthReport>();
    /**
     * Is this Job in the build queue
     */
    private boolean inQueue;
    /**
     * No idea
     */
    private boolean keepDependencies;
    /**
     * Last build which could be completed
     */
    private Build lastCompletedBuild;
    /**
     * The last build which was triggered
     */
    private Build lastBuild;
    /**
     * Last build which failed
     */
    private Build lastFailedBuild;
    /**
     * Last build which had a stable status after the build
     */
    private Build lastStableBuild;
    /**
     * Last build which was successful
     */
    private Build lasSuccessfulBuild;
    /**
     * Last build which had an unstable status
     */
    private Build lastUnstableBuild;
    /**
     * Last build which han an unsuccessful status
     */
    private Build lastUnsuccessfulBuild;
    /**
     * The next triggered build will be assigned this number
     */
    private long nextBuildNumber;
    /**
     * Is this item in the current build queue
     */
    private boolean queueItem;
    /**
     * Is this build concurrent?
     */
    private boolean concurrentBuild;
    /**
     * Projects which will be triggered when this build is finished
     */
    private List<Job> downstreamProjects = new ArrayList<Job>();
    /**
     * After the build of this projects finishes, this job will be triggered
     */
    private List<Job> upstreamProjects = new ArrayList<Job>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayNameOrNull() {
        return displayNameOrNull;
    }

    public void setDisplayNameOrNull(String displayNameOrNull) {
        this.displayNameOrNull = displayNameOrNull;
    }

    public boolean isBuildable() {
        return buildable;
    }

    public void setBuildable(boolean buildable) {
        this.buildable = buildable;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public List<Action> getProperty() {
        return property;
    }

    public void setProperty(List<Action> property) {
        this.property = property;
    }

    public List<Build> getBuilds() {
        return builds;
    }

    public void setBuilds(List<Build> builds) {
        this.builds = builds;
    }

    public Build getFirstBuild() {
        return firstBuild;
    }

    public void setFirstBuild(Build firstBuild) {
        this.firstBuild = firstBuild;
    }

    public List<HealthReport> getHealthReport() {
        return healthReport;
    }

    public void setHealthReport(List<HealthReport> healthReport) {
        this.healthReport = healthReport;
    }

    public boolean isInQueue() {
        return inQueue;
    }

    public void setInQueue(boolean inQueue) {
        this.inQueue = inQueue;
    }

    public boolean isKeepDependencies() {
        return keepDependencies;
    }

    public void setKeepDependencies(boolean keepDependencies) {
        this.keepDependencies = keepDependencies;
    }

    public Build getLastCompletedBuild() {
        return lastCompletedBuild;
    }

    public void setLastCompletedBuild(Build lastCompletedBuild) {
        this.lastCompletedBuild = lastCompletedBuild;
    }

    public Build getLastBuild() {
        return lastBuild;
    }

    public void setLastBuild(Build lasBuild) {
        this.lastBuild = lasBuild;
    }

    public Build getLastFailedBuild() {
        return lastFailedBuild;
    }

    public void setLastFailedBuild(Build lastFailedBuild) {
        this.lastFailedBuild = lastFailedBuild;
    }

    public Build getLastStableBuild() {
        return lastStableBuild;
    }

    public void setLastStableBuild(Build lastStableBuild) {
        this.lastStableBuild = lastStableBuild;
    }

    public Build getLasSuccessfulBuild() {
        return lasSuccessfulBuild;
    }

    public void setLasSuccessfulBuild(Build lasSuccessfulBuild) {
        this.lasSuccessfulBuild = lasSuccessfulBuild;
    }

    public Build getLastUnstableBuild() {
        return lastUnstableBuild;
    }

    public void setLastUnstableBuild(Build lastUnstableBuild) {
        this.lastUnstableBuild = lastUnstableBuild;
    }

    public Build getLastUnsuccessfulBuild() {
        return lastUnsuccessfulBuild;
    }

    public void setLastUnsuccessfulBuild(Build lastUnsuccessfulBuild) {
        this.lastUnsuccessfulBuild = lastUnsuccessfulBuild;
    }

    public long getNextBuildNumber() {
        return nextBuildNumber;
    }

    public void setNextBuildNumber(long nextBuildNumber) {
        this.nextBuildNumber = nextBuildNumber;
    }

    public boolean isQueueItem() {
        return queueItem;
    }

    public void setQueueItem(boolean queueItem) {
        this.queueItem = queueItem;
    }

    public boolean isConcurrentBuild() {
        return concurrentBuild;
    }

    public void setConcurrentBuild(boolean concurrentBuild) {
        this.concurrentBuild = concurrentBuild;
    }

    public List<Job> getDownstreamProjects() {
        return downstreamProjects;
    }

    public void setDownstreamProjects(List<Job> downstreamProjects) {
        this.downstreamProjects = downstreamProjects;
    }

    public List<Job> getUpstreamProjects() {
        return upstreamProjects;
    }

    public void setUpstreamProjects(List<Job> upstreamProjects) {
        this.upstreamProjects = upstreamProjects;
    }

    /**
     * represents an action
     */
    private class Action implements JenkinsEntity {

        /**
         * Parameter definition
         */
        private List<ParameterDefinition> parameterDefinitions = new ArrayList<ParameterDefinition>();

        public List<ParameterDefinition> getParameterDefinitions() {
            return parameterDefinitions;
        }

        public void setParameterDefinitions(List<ParameterDefinition> parameterDefinitions) {
            this.parameterDefinitions = parameterDefinitions;
        }
    }
}
