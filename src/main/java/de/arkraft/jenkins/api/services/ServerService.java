package de.arkraft.jenkins.api.services;

import de.arkraft.jenkins.api.entities.JenkinsServer;
import de.arkraft.jenkins.api.entities.JobQueue;
import retrofit.http.GET;
import retrofit.http.POST;


/**
 * This service allows to control a Jenkins server
 * The following actions are possible:
 * <ul>
 *  <li>Get the Server Object with information about the views and jobs</li>
 *  <li>Quiet down the server. If you do not want to shutdown the server but also want to deactivate building for all
 *  jobs at once, you can quiet down the server.</li>
 *  <li>Cancel the quiet down</li>
 *  <li>Get the job queue</li>
 *  <li>Get the Executors for this server instance</li>
 * </ul>
 *
 * @author Artur Kraft
 */
public interface ServerService {

    /**
     * Returns the Server Object.
     *
     * @return the server object
     */
    @GET("/api/json")
    public JenkinsServer getServer();

    /**
     * Quiet down the Server
     */
    @POST("/quietDown")
    public void quietDown();

    /**
     * Cancel the quiet down
     */
    @POST("/cancelQuietDown")
    public void cancelQuietDown();

    /**
     * Get the JobQueue. It holds all currently building jobs
     *
     * @return JobQueue
     */
    @GET("/queue/api/json")
    public JobQueue getQueue();

}
