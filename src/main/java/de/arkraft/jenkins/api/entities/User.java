package de.arkraft.jenkins.api.entities;

import de.arkraft.jenkins.api.JenkinsEntity;

/**
 * Representing a User node in Jenkins
 *
 * @author Artur Kraft
 */
public class User implements JenkinsEntity {

    private String fullName;
    private String absoluteUrl;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAbsoluteUrl() {
        return absoluteUrl;
    }

    public void setAbsoluteUrl(String absoluteUrl) {
        this.absoluteUrl = absoluteUrl;
    }
}
