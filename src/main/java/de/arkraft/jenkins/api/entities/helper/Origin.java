package de.arkraft.jenkins.api.entities.helper;

import de.arkraft.jenkins.api.JenkinsEntity;

/**
 * Created by Artur on 30.05.2014.
 */
public class Origin implements JenkinsEntity {

    private long buildNumber;
    private String buildResult;
    private BuildRevision marked;
    private BuildRevision revision;

    public long getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(long buildNumber) {
        this.buildNumber = buildNumber;
    }

    public String getBuildResult() {
        return buildResult;
    }

    public void setBuildResult(String buildResult) {
        this.buildResult = buildResult;
    }

    public BuildRevision getMarked() {
        return marked;
    }

    public void setMarked(BuildRevision marked) {
        this.marked = marked;
    }

    public BuildRevision getRevision() {
        return revision;
    }

    public void setRevision(BuildRevision revision) {
        this.revision = revision;
    }
}
