package de.arkraft.jenkins.api.entities;

import de.arkraft.jenkins.api.JenkinsEntity;

/**
 * Utilization statistics for a node or a set of nodes.
 *
 * @author Artur Kraft
 */
public class LoadStatistics implements JenkinsEntity {

    private MultiStageTimeSeries busyExecutors;

    private MultiStageTimeSeries queueLength;

    private MultiStageTimeSeries totalExecutors;

    public MultiStageTimeSeries getBusyExecutors() {
        return busyExecutors;
    }

    public void setBusyExecutors(MultiStageTimeSeries busyExecutors) {
        this.busyExecutors = busyExecutors;
    }

    public MultiStageTimeSeries getQueueLength() {
        return queueLength;
    }

    public void setQueueLength(MultiStageTimeSeries queueLength) {
        this.queueLength = queueLength;
    }

    public MultiStageTimeSeries getTotalExecutors() {
        return totalExecutors;
    }

    public void setTotalExecutors(MultiStageTimeSeries totalExecutors) {
        this.totalExecutors = totalExecutors;
    }

    /**
     * Maintains several {@link TimeSeries} with different update frequencies to satisfy three goals;
     * <ol>
     *     <li>retain data over long timespan</li>
     *     <li>save memory</li>
     *     <li>retain accurate data for the recent past</li>
     * </ol>
     *
     * Original comment from Jenkins-CI source by Kohsuke Kawaguchi
     * <a href="https://github.com/jenkinsci/jenkins/blob/master/core/src/main/java/hudson/model/MultiStageTimeSeries.java">MultiStageTimeSeries.java</a>
     *
     * @author Artur Kraft
     */
    public class MultiStageTimeSeries implements JenkinsEntity {

        /**
         * Updated every 10 seconds. Keep data up to 1 hour.
         */
        public TimeSeries sec10;
        /**
         * Updated every 1 min. Keep data up to 1 day.
         */
        public TimeSeries min;
        /**
         * Updated every 1 hour. Keep data up to 4 weeks.
         */
        public TimeSeries hour;

        public TimeSeries getSec10() {
            return sec10;
        }

        public void setSec10(TimeSeries sec10) {
            this.sec10 = sec10;
        }

        public TimeSeries getMin() {
            return min;
        }

        public void setMin(TimeSeries min) {
            this.min = min;
        }

        public TimeSeries getHour() {
            return hour;
        }

        public void setHour(TimeSeries hour) {
            this.hour = hour;
        }

        /**
         * Scalar value that changes over the time (such as load average, Q length, # of executors, etc.)
         *
         * Original Comment from Jenkins-CI source by Kohsuke Kawaguchi
         * <a href="https://github.com/jenkinsci/jenkins/blob/master/core/src/main/java/hudson/model/TimeSeries.java">TimeSeries.java</a>
         *
         * @author Artur Kraft
         */
        public class TimeSeries implements JenkinsEntity {
            /**
             * Decay ratio. Normally 1-e for some small e.
             */
            private float decay;

            /**
             * Historical exponential moving average data. Newer ones first.
             */
            private float[] history;

            /**
             * Maximum history size.
             */
            private int historySize;

            public float getDecay() {
                return decay;
            }

            public void setDecay(float decay) {
                this.decay = decay;
            }

            public float[] getHistory() {
                return history;
            }

            public void setHistory(float[] history) {
                this.history = history;
            }

            public int getHistorySize() {
                return historySize;
            }

            public void setHistorySize(int historySize) {
                this.historySize = historySize;
            }
        }

    }

}
