package de.arkraft.jenkins.api.entities.helper;

/**
 * Git Branch object
 */
public class Branch {

    /**
     * Git sha1 id
     */
    private String SHA1;
    /**
     * Name
     */
    private String name;

    public String getSHA1() {
        return SHA1;
    }

    public void setSHA1(String SHA1) {
        this.SHA1 = SHA1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
