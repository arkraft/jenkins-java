package de.arkraft.jenkins.api.entities;

import de.arkraft.jenkins.api.JenkinsEntity;

/**
 * Thread that executes builds.
 *
 * @author Artur Kraft
 */
public class Executor implements JenkinsEntity {

    private Build currentExecutable;
    private WorkUnit currentWorkUnit;
    private boolean idle;
    private boolean likelyStuck;
    private long number;
    private long progress;

    public Build getCurrentExecutable() {
        return currentExecutable;
    }

    public void setCurrentExecutable(Build currentExecutable) {
        this.currentExecutable = currentExecutable;
    }

    public WorkUnit getCurrentWorkUnit() {
        return currentWorkUnit;
    }

    public void setCurrentWorkUnit(WorkUnit currentWorkUnit) {
        this.currentWorkUnit = currentWorkUnit;
    }

    public boolean isIdle() {
        return idle;
    }

    public void setIdle(boolean idle) {
        this.idle = idle;
    }

    public boolean isLikelyStuck() {
        return likelyStuck;
    }

    public void setLikelyStuck(boolean likelyStuck) {
        this.likelyStuck = likelyStuck;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public long getProgress() {
        return progress;
    }

    public void setProgress(long progress) {
        this.progress = progress;
    }
}
