package de.arkraft.jenkins.api.services;

import de.arkraft.jenkins.api.entities.Build;
import de.arkraft.jenkins.api.entities.Job;
import de.arkraft.jenkins.api.entities.TestReport;
import retrofit.client.Response;
import retrofit.http.*;

import java.nio.file.attribute.AclEntry;
import java.util.Map;

/**
 * Provides methods to read and manipulate job data like the job or build configuration
 *
 * @author Artur Kraft
 */
public interface JobService {

    @GET("/job/{name}/api/json")
    public Job getJobByName(@Path("name") String jobName);

    @GET("/job/{name}/{number}/api/json")
    public Build getBuild(@Path("name") String jobName, @Path("number") String buildNumber);

    /**
     * Trigger a build by the job name
     *
     * @param jobName Name of the Job you want to build
     * @return an empty response
     */
    @GET("/job/{name}/build")
    public Response triggerBuild(@Path("name") String jobName);

    /**
     * Trigger a secure build by the job name. For secured servers you need to provide the job token
     *
     * @param jobName  Name of the Job you want to build
     * @param jobToken Token of the job you want to build
     * @return an empty Response
     */
    @POST("/job/{name}/build")
    public Response triggerBuild(@Path("name") String jobName, @Query("token") String jobToken);

    /**
     * Toogle keeping log forever. If log is not kept, it will be deleted after some time.
     *
     * @param jobName     Name of the Job
     * @param buildNumber Number of the build of which you want to keep the log
     * @return HTTP Response, 200 if everything went fine
     */
    @GET("/job/{job}/build/{number}/toggleLogKeep")
    public Response toggleLogKeep(@Path("job") String jobName, @Path("number") String buildNumber);

    /**
     * Rename a job
     *
     * @param oldName job name to rename
     * @param newName new name of the job
     * @return HTTP Response
     */
    @POST("/job/{old_name}/doRename")
    public Response rename(@Path("old_name") String oldName, @Query("newName") String newName);

    /**
     * Delete a job with the given job name
     *
     * @param toDelete job name og the job to delete
     * @return HTTP Response
     */
    @POST("/job/{name}/doDelete")
    public Response delete(@Path("name") String toDelete);

    /**
     * Wipe out workspace
     *
     * @param jobName name of the job which workspace should be wiped out
     * @return HTTP Response
     */
    @POST("/job/{name}/doWipeOutWorkspace")
    public Response wipeOutWorkspace(@Path("name") String jobName);

    /**
     * Stop the latest build of a job
     *
     * @param toStop name of the job to be stopped
     * @return HTTP Response
     */
    @POST("/job/{name}/lastBuild/stop")
    public Response stop(@Path("name") String toStop);

    /**
     * Stop a build of a job
     *
     * @param toStop Name of the job to stop
     * @param build  Build number
     * @return HTTP Response
     */
    @POST("/job/{name}/{build_number}/stop")
    public Response stop(@Path("name") String toStop, @Path("build_number") String build);

    /**
     * Copy a job to create a new job
     *
     * @param newJobName Name of the new job created through copy
     * @param copyFrom   Name of the job to copy in order to create a new job
     * @return HTTP Response
     */
    @POST("/createItem?mode=copy")
    public Response copy(@Query("name") String newJobName, @Query("from") String copyFrom);

    /**
     * Get build log as text
     *
     * @param job         name of the job
     * @param buildNumber number of the build
     * @return HTTP Response
     */
    @GET("/job/{name}/{build_number}/logText/progressivetext")
    public Response getLog(@Path("name") String job, @Path("build_number") String buildNumber);

    /**
     * Get build log as html
     *
     * @param job         name of the job
     * @param buildNumber number of the build
     * @return HTTP Response
     */
    @GET("/job/{name}/{build_number}/logText/progressivehtml")
    public Response getLogAsHtml(@Path("name") String job, @Path("build_number") String buildNumber);

    /**
     * Get build log as text by starting a build
     *
     * @param job         name of the job
     * @param buildNumber number of the build
     * @return HTTP Response
     */
    @GET("/job/{name}/{build_number}/logText/progressivetext?start=true")
    public Response getLogAfterStart(@Path("name") String job, @Path("build_number") String buildNumber);

    /**
     * Create a new job by providing a configuration XML
     *
     * @param name             name of the new job
     * @param configurationXml Configuration file as String
     * @return HTTP Response
     */
    @Headers({
            "Content-Type: application/xml"
    })
    @POST("/createItem")
    public Response createJob(@Query("name") String name, @Body String configurationXml);

    /**
     * Updates a job by providing a configuration XML
     *
     * @param name             of the job to update
     * @param configurationXml new configuration as String
     * @return HTT Response
     */
    @Headers({
            "Content-type: application/xml"
    })
    @POST("/job/{name}/config.xml")
    public Response updateJob(@Path("name") String name, @Body String configurationXml);

    /**
     * Build a job
     *
     * @param name of the job to build
     * @return HTTP Response
     */
    @POST("/job/{name}/build")
    public Response build(@Path("name") String name);

    /**
     * Build a job with parameters
     *
     * @param name       of the job to build
     * @param parameters Map of parameters for the build
     * @return HTTP Response
     */
    @POST("/job/{name}/buildWithParameters")
    public Response build(@Path("name") String name, @QueryMap Map<String, String> parameters);

    /**
     * Enable a job
     *
     * @param name of the job
     * @return HTTP Response
     */
    @POST("/job/{name}/enable")
    public Response enable(@Path("name") String name);

    /**
     * Disable a job
     *
     * @param name of the job
     * @return HTTP Response
     */
    @POST("/job/{name}/disable")
    public Response disable(@Path("name") String name);

    /**
     * Get the job configuration of a specified job
     *
     * @param name of the job to get the configuration from
     * @return HTTP Response
     */
    @GET("/job/{name}/config.xml")
    public Response getConfig(@Path("name") String name);

    /**
     * Get test report for a specified job
     *
     * @param name    of the job
     * @param buildNr number of the build
     * @return Test reposrt
     */
    @GET("/job/{name}/{build}/testReport")
    public TestReport getTestReport(@Path("name") String name, @Path("build") String buildNr);

}
