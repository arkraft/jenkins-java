package de.arkraft.jenkins.api.entities;

import de.arkraft.jenkins.api.JenkinsEntity;
import de.arkraft.jenkins.api.entities.helper.Action;
import de.arkraft.jenkins.api.entities.helper.BuildAction;
import de.arkraft.jenkins.api.entities.helper.CauseAction;
import de.arkraft.jenkins.api.entities.helper.TestReportAction;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a Jenkins Build object. It hold the information about a build, for example if this build is
 * still in execution or information about the build result.
 *
 * @author Artur Kraft
 */
public class Build implements JenkinsEntity {

    /**
     * Number of the build
     */
    private long number;
    /**
     * url to call the build in a webbrowser
     */
    private String url;
    private List<Action> actions = new ArrayList<Action>();
    private List<Artifact> artifacts = new ArrayList<Artifact>();
    /**
     * Is job building right now?
     */
    private boolean building;
    private String description;
    /**
     * Duration of the build
     */
    private long duration;
    /**
     * Duration calculated by using duration of other builds
     */
    private long estimatedDuration;
    //TODO: check if it is a computer!!
    private Node executor;
    private String fullDisplayName;
    private String id;
    private boolean keepLog;
    private String result;
    private long timestamp;
    private String builtOn;
    private List<User> culprits = new ArrayList<User>();
    //TODO: maven artifacts?
    private String mavenVersionUsed;

    private ChangeSet changeSet;

    /**
     * Return build number
     *
     * @return build number
     */
    public long getNumber() {
        return number;
    }

    /**
     * Set build number
     *
     * @param number new build number
     */
    public void setNumber(long number) {
        this.number = number;
    }

    /**
     * Get url to open in browser
     *
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Set url
     *
     * @param url new value
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Return build artifacts
     *
     * @return build artifacts
     */
    public List<Artifact> getArtifacts() {
        return artifacts;
    }

    /**
     * Set build artifacts
     *
     * @param artifacts new value
     */
    public void setArtifacts(List<Artifact> artifacts) {
        this.artifacts = artifacts;
    }

    /**
     * Is this build running right now
     *
     * @return true if the job is building right now
     */
    public boolean isBuilding() {
        return building;
    }

    /**
     * Set true if the job is building right now
     *
     * @param building true if building
     */
    public void setBuilding(boolean building) {
        this.building = building;
    }

    /**
     * Returns description
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set description
     *
     * @param description new value
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Returns duration
     *
     * @return duration
     */
    public long getDuration() {
        return duration;
    }

    /**
     * Set duration
     *
     * @param duration new value
     */
    public void setDuration(long duration) {
        this.duration = duration;
    }

    /**
     * Returns estimated duration
     *
     * @return estimated duration
     */
    public long getEstimatedDuration() {
        return estimatedDuration;
    }

    /**
     * Set estimated duration
     *
     * @param estimatedDuration new value
     */
    public void setEstimatedDuration(long estimatedDuration) {
        this.estimatedDuration = estimatedDuration;
    }

    /**
     * Returns executor
     * TODO: Check if it really is a computer
     *
     * @return executor
     */
    public Node getExecutor() {
        return executor;
    }

    /**
     * Set executor
     *
     * @param executor new value
     */
    public void setExecutor(Node executor) {
        this.executor = executor;
    }

    /**
     * Returns full display name
     *
     * @return full display name
     */
    public String getFullDisplayName() {
        return fullDisplayName;
    }

    /**
     * Set full display name
     *
     * @param fullDisplayName new name
     */
    public void setFullDisplayName(String fullDisplayName) {
        this.fullDisplayName = fullDisplayName;
    }

    /**
     * Returns build id
     *
     * @return build id
     */
    public String getId() {
        return id;
    }

    /**
     * Set build id
     *
     * @param id new id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Is keepLog activated
     *
     * @return true if the log is kept
     */
    public boolean isKeepLog() {
        return keepLog;
    }

    /**
     * Set keep log
     *
     * @param keepLog true if logs should be kept
     */
    public void setKeepLog(boolean keepLog) {
        this.keepLog = keepLog;
    }

    /**
     * Returns status of the build
     *
     * @return status of the build
     */
    public String getResult() {
        return result;
    }

    /**
     * Set build result
     *
     * @param result new result
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * Returns timestamp of build start
     *
     * @return timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Set timestamp
     *
     * @param timestamp new value
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Name of the machine the build was executed on
     *
     * @return name of the building machine
     */
    public String getBuiltOn() {
        return builtOn;
    }

    /**
     * Set name of the building machine
     *
     * @param builtOn machine name
     */
    public void setBuiltOn(String builtOn) {
        this.builtOn = builtOn;
    }

    /**
     * Get list of the users connected to the job
     *
     * @return list of culprits
     */
    public List<User> getCulprits() {
        return culprits;
    }

    /**
     * Set list of culprits
     *
     * @param culprits new list
     */
    public void setCulprits(List<User> culprits) {
        this.culprits = culprits;
    }

    /**
     * Get version of maven used to build the project
     *
     * @return maven version
     */
    public String getMavenVersionUsed() {
        return mavenVersionUsed;
    }

    /**
     * Set maven version
     *
     * @param mavenVersionUsed version
     */
    public void setMavenVersionUsed(String mavenVersionUsed) {
        this.mavenVersionUsed = mavenVersionUsed;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public ChangeSet getChangeSet() {
        return changeSet;
    }

    public void setChangeSet(ChangeSet changeSet) {
        this.changeSet = changeSet;
    }

    public BuildAction getBuildAction() {
        if(actions.get(1) != null && actions.get(1) instanceof BuildAction) {
            return (BuildAction) actions.get(1);
        }
        for (Action action : actions) {
            if(action instanceof BuildAction) {
                return (BuildAction) action;
            }
        }
        return null;
    }

    public TestReportAction getTestReportAction() {
        if(actions.get(4) != null && actions.get(4) instanceof TestReportAction) {
            return (TestReportAction)actions.get(4);
        }
        for (Action action : actions) {
            if(action instanceof TestReportAction) {
                return (TestReportAction)action;
            }
        }
        return null;
    }

    public CauseAction getCauseAction() {
        if(actions.get(0) != null && actions.get(0) instanceof CauseAction) {
            return (CauseAction) actions.get(0);
        }
        for (Action action : actions) {
            if(action instanceof CauseAction) {
                return (CauseAction)action;
            }
        }
        return null;
    }

}
