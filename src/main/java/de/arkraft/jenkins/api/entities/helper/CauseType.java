package de.arkraft.jenkins.api.entities.helper;

/**
 * Created by Artur on 11.05.2014.
 */
public enum CauseType {

    LEGACY_CODE,
    UPSTREAM,
    REMOTE,
    USERID,
    USER;

}
