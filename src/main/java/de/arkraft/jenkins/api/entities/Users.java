package de.arkraft.jenkins.api.entities;

import de.arkraft.jenkins.api.JenkinsEntity;

import java.util.List;

/**
 * Created by Artur on 30.05.2014.
 */
public class Users implements JenkinsEntity {

    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
