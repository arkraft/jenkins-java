package de.arkraft.jenkins.api.entities;

import com.google.gson.annotations.SerializedName;
import de.arkraft.jenkins.api.JenkinsEntity;
import de.arkraft.jenkins.api.entities.helper.Action;

import java.util.ArrayList;
import java.util.List;

/**
 * An entity that describes a remote Computer on which builds are performed.
 *
 * @author Artur Kraft
 */
public class Node implements JenkinsEntity {

    private List<Action> actions = new ArrayList<Action>();
    private String displayName;
    private List<Executor> executors = new ArrayList<Executor>();
    private String icon;
    private boolean idle;
    private boolean jnlpAgent;
    private boolean launchSupported;
    private LoadStatistics loadStatistics;
    private boolean manualLaunchAllowed;
    private MonitorData monitorData;
    @SerializedName("numExecutors")
    private int numberOfExecutors;
    private boolean offline;
    //TODO: find out what a cause look like
    //private String offlineCause;
    private String offlineCauseReason;
    private List<String> oneOffExecutors;
    private boolean temporarilyOffline;

    /**
     * Get the display name
     *
     * @return {@link #displayName}
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Set the display name
     *
     * @param displayName new value
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Get the path to the icon
     *
     * @return {@link #icon}
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Set the icon path
     *
     * @param icon new path to icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * Is idle
     *
     * @return idle
     */
    public boolean isIdle() {
        return idle;
    }

    /**
     * Set idle
     *
     * @param idle new value
     */
    public void setIdle(boolean idle) {
        this.idle = idle;
    }

    /**
     * Is jnlp agent
     *
     * @return jnlpAgent
     */
    public boolean isJnlpAgent() {
        return jnlpAgent;
    }

    /**
     * Set jnlp agent
     *
     * @param jnlpAgent is a jnlp agent
     */
    public void setJnlpAgent(boolean jnlpAgent) {
        this.jnlpAgent = jnlpAgent;
    }

    /**
     * Is launch supported
     *
     * @return launchSupported
     */
    public boolean isLaunchSupported() {
        return launchSupported;
    }

    /**
     * Set if launch is supported
     *
     * @param launchSupported true is it is supported
     */
    public void setLaunchSupported(boolean launchSupported) {
        this.launchSupported = launchSupported;
    }

    /**
     * Is manual launch allowed
     *
     * @return manualLaunchAllowed
     */
    public boolean isManualLaunchAllowed() {
        return manualLaunchAllowed;
    }

    /**
     * Set manualLaunchAllowed
     *
     * @param manualLaunchAllowed true if it is allowed
     */
    public void setManualLaunchAllowed(boolean manualLaunchAllowed) {
        this.manualLaunchAllowed = manualLaunchAllowed;
    }

    /**
     * Monitoring data
     *
     * @return monitorData
     */
    public MonitorData getMonitorData() {
        return monitorData;
    }

    /**
     * Set the monitoring data
     *
     * @param monitorData new value
     */
    public void setMonitorData(MonitorData monitorData) {
        this.monitorData = monitorData;
    }

    /**
     * Get the Number of executors for this computer
     *
     * @return {@link #numberOfExecutors}
     */
    public int getNumberOfExecutors() {
        return numberOfExecutors;
    }

    /**
     * Set the number of Executors
     *
     * @param numberOfExecutors new value
     */
    public void setNumberOfExecutors(int numberOfExecutors) {
        this.numberOfExecutors = numberOfExecutors;
    }

    /**
     * Is this computer offline
     *
     * @return offline
     */
    public boolean isOffline() {
        return offline;
    }

    /**
     * Set the computers offline state
     *
     * @param offline new value
     */
    public void setOffline(boolean offline) {
        this.offline = offline;
    }

    /**
     * Get the reason why this computer is offline
     *
     * @return offlineCauseReason
     */
    public String getOfflineCauseReason() {
        return offlineCauseReason;
    }

    /**
     * Set the reason why this computer is offline
     *
     * @param offlineCauseReason
     */
    public void setOfflineCauseReason(String offlineCauseReason) {
        this.offlineCauseReason = offlineCauseReason;
    }

    /**
     * Returns oneOffExecutors
     *
     * @return oneOffExecutors
     */
    public List<String> getOneOffExecutors() {
        return oneOffExecutors;
    }

    /**
     * Set oneOffExecutors
     *
     * @param oneOffExecutors new value
     */
    public void setOneOffExecutors(List<String> oneOffExecutors) {
        this.oneOffExecutors = oneOffExecutors;
    }

    /**
     * Whether this computer is temporarily offline or not
     *
     * @return temporarilyOffline
     */
    public boolean isTemporarilyOffline() {
        return temporarilyOffline;
    }

    /**
     * Set the computers temporarily offline state
     *
     * @param temporarilyOffline new value
     */
    public void setTemporarilyOffline(boolean temporarilyOffline) {
        this.temporarilyOffline = temporarilyOffline;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public List<Executor> getExecutors() {
        return executors;
    }

    public void setExecutors(List<Executor> executors) {
        this.executors = executors;
    }

    public LoadStatistics getLoadStatistics() {
        return loadStatistics;
    }

    public void setLoadStatistics(LoadStatistics loadStatistics) {
        this.loadStatistics = loadStatistics;
    }
}
